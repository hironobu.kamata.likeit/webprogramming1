<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除</title>
</head>
<body>

	<div>
		<p>${userInfo.name} さん</p>
		<a href="logout_servlet">ログアウト</a>
	</div>

	<h1>ユーザー削除確認</h1>

	<p>ユーザーID : ID ${userDate.id}</p>
	<p>本当に削除してよろしいでしょうか？</p>

	<form action="DeleteServlet" method="post">
		<input type="hidden" name="id" value="${userDate.id}">
		<input type="submit" name="delete" value="OK">
	</form>
		<input type="submit" name="cancel" value="キャンセル" onclick="location.href='UserListServlet'">

	<a href="UserListServlet">戻る</a>

</body>
</html>