package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	//　ログイン情報　------------------

	public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;

        password = PassMD(password);

        try {
            // データベースへ接続
            conn = DBManagement.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }
        	// 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");

            String nameData = rs.getString("name");

            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

// ----------------------------------------------------------------------

    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManagement.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = " SELECT * FROM user WHERE login_id not in ('admin'); ";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String birthDate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }


    /**
     * 検索ワードに一致したユーザ情報を取得する
     * @return
     */
    public List<User> find(String login_id, String nameSearch, String birthdateFrom, String birthDateTo) {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManagement.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = " SELECT * FROM user WHERE login_id not in ('admin')";

            if(!(login_id.isEmpty())) {
            	sql += " AND login_id = '" + login_id + "'";
            }

            if(!(nameSearch.isEmpty())) {
            	sql += " AND name LIKE '%" + nameSearch + "%'";
            }

            if( !(birthdateFrom.isEmpty())) {
            	sql += " AND birth_date >=" + "'" +birthdateFrom + "'";
            }

            if( !(birthDateTo.isEmpty())) {
            	sql += " AND birth_date <=" + "'" +birthDateTo + "'";
            }


//          BETWEEN 下限 AND 上限

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String birthDate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

 // ----------------------------------------------------------------------
 // 新規登録	-------------
    public void newUser(String login_id, String password, String name, String birth_date) {

	Connection conn = null;
	PreparedStatement stmt = null;

	password = PassMD(password);

    try {
        // データベースへ接続
        conn = DBManagement.getConnection();
        // SELECT文を準備
        String sql =
        "INSERT INTO user(login_id, password, name, birth_date, create_date, update_date) VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);";

        // SELECTを実行し、結果表を取得
        stmt = conn.prepareStatement(sql);
        stmt.setString(1, login_id);
        stmt.setString(2, password);
        stmt.setString(3, name);
        stmt.setString(4, birth_date);

        stmt.executeUpdate();

//    	if (stmt != null) {}
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

// ログインid確認用　------------------
public User findByNew(String loginId) {
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManagement.getConnection();

        // SELECT文を準備
        String sql = "SELECT login_id FROM user WHERE login_id = ?";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, loginId);
        ResultSet rs = pStmt.executeQuery();

        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }

        // 必要なデータのみインスタンスのフィールドに追加
        String loginIdData = rs.getString("login_id");
        return new User(loginIdData);

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

//----------------------------------------------------------------------

public User findByMoreList(int id) {
	Connection conn = null;

    try {
        // データベースへ接続
        conn = DBManagement.getConnection();
        // SELECT文を準備
        String sql = "SELECT login_id, name, birth_date, create_date, update_date, id FROM user WHERE id = ?";

        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, id);
        ResultSet rs = pStmt.executeQuery();

        if (!rs.next()) {
            return null;
        }

     // 必要なデータのみインスタンスのフィールドに追加
        int idDate = rs.getInt("id");
        String loginIdData = rs.getString("login_id");
        String nameData = rs.getString("name");
        String birthData = rs.getString("birth_date");
        String createData = rs.getString("create_date");
        String upData = rs.getString("update_date");

        User userDate = new User(idDate, loginIdData, nameData, birthData, createData, upData);
        return userDate;

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

// ユーザー情報更新 ----------------------------------------------------------------

public void upDateUser(String password, String name, String birth_date, int id) {

	Connection conn = null;
	PreparedStatement stmt = null;

	password = PassMD(password);

    try {
        // データベースへ接続
        conn = DBManagement.getConnection();
        // SELECT文を準備
        String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = CURRENT_TIMESTAMP WHERE id = ?;";

        // SELECTを実行し、結果表を取得
        stmt = conn.prepareStatement(sql);
        stmt.setString(1, password);
        stmt.setString(2, name);
        stmt.setString(3, birth_date);
        stmt.setInt(4, id);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

public void upDateUser2(String name, String birth_date, int id) {

	Connection conn = null;
	PreparedStatement stmt = null;

    try {
        // データベースへ接続
        conn = DBManagement.getConnection();
        // SELECT文を準備
        String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = CURRENT_TIMESTAMP WHERE id = ?;";

        // SELECTを実行し、結果表を取得
        stmt = conn.prepareStatement(sql);
        stmt.setString(1, name);
        stmt.setString(2, birth_date);
        stmt.setInt(3, id);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

// ユーザー削除用	  ----------------------------------------------------------------
public void deleteUser(int id) {

	Connection conn = null;
	PreparedStatement stmt = null;

    try {
        // データベースへ接続
        conn = DBManagement.getConnection();
        // SELECT文を準備
        String sql = "DELETE FROM user WHERE id = ?;";

        // SELECTを実行し、結果表を取得
        stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

//　ハッシュ化　------------------------------------

public String PassMD(String pass_md) {

	//ハッシュを生成したい元の文字列
	String source = pass_md;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = null;

	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
	}

	String result = DatatypeConverter.printHexBinary(bytes);

	return result;

}

}

