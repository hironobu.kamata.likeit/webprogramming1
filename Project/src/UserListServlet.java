

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String USER_LIST = "/WEB-INF/jsp/user-list.jsp";
	public static final String MORE_USER = "/WEB-INF/jsp/more-user.jsp";
	public static final String INDEX = "/WEB-INF/jsp/index.jsp";

    public UserListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		List<User> user = userDao.findAll();

// -----------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("login_servlet");
			return;
		}
// ------------------------------------------------------------------
		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher(USER_LIST);
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String login_id = request.getParameter("login_id");
		String name = request.getParameter("name");
		String birth_date1 = request.getParameter("date1");
		String birth_date2 = request.getParameter("date2");

		UserDao userDao = new UserDao();
		List<User> user = userDao.find(login_id, name, birth_date1, birth_date2);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher(USER_LIST);
		dispatcher.forward(request, response);

	}
}

