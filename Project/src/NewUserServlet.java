
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/NewUserServlet")
public class NewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String NEW_USER = "/WEB-INF/jsp/new-user.jsp";
	public static final String USER_LIST = "/WEB-INF/jsp/user-list.jsp";

    public NewUserServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// ------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("login_servlet");
			return;
		}
// ------------------------------------------------------------------

		RequestDispatcher dispatcher = request.getRequestDispatcher(NEW_USER);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		 // リクエストパラメータの入力項目を取得
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");

		//-------------
		if(!(password.equals(password2))) {
			request.setAttribute("errMsg", "パスワードが違います。");
			//	jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(NEW_USER);
			dispatcher.forward(request, response);
			return;
		}

		//-------------
		if(login_id.equals("") || name.equals("") || password.equals("") || password2.equals("") || name.equals("") || birth_date.equals("")) {
			request.setAttribute("errMsg", "入力されていない項目があります。");
			//	jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(NEW_USER);
			dispatcher.forward(request, response);
			return;
		}

		//-------------
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao2 = new UserDao();
		User user = userDao2.findByNew(login_id);

		if(user != null) {
			request.setAttribute("errMsg", "入力された値が違います。");
			//	jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(NEW_USER);
			dispatcher.forward(request, response);

			return;
		}

		//-------------
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		userDao.newUser(login_id, password, name, birth_date);

		response.sendRedirect("UserListServlet");

    }
}

