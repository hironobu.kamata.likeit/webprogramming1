

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UpdateUserServlet")
public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String UPDATE_USER = "/WEB-INF/jsp/update-user.jsp";

    public UpdateUserServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

// ------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("login_servlet");
			return;
		}
// ------------------------------------------------------------------

		int id = Integer.parseInt(request.getParameter("id"));

//		リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		User userDate = userDao.findByMoreList(id);

//		request.getAttribute("id");

		HttpSession session2 = request.getSession();
		session2.setAttribute("userDate", userDate);

		RequestDispatcher dispatcher = request.getRequestDispatcher(UPDATE_USER);
		dispatcher.forward(request, response);


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
        int id = Integer.parseInt(request.getParameter("id"));
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");

	//	------------
		//　保留
//		//ハッシュを生成したい元の文字列
//		String source = password;
//		//ハッシュ生成前にバイト配列に置き換える際のCharset
//		Charset charset = StandardCharsets.UTF_8;
//		//ハッシュアルゴリズム
//		String algorithm = "MD5";
//
//		//ハッシュ生成処理
//		byte[] bytes = null;
//		try {
//			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
//		} catch (NoSuchAlgorithmException e) {
//			// TODO 自動生成された catch ブロック
//			e.printStackTrace();
//		}
//		String result = DatatypeConverter.printHexBinary(bytes);
//		//標準出力
//		System.out.println(result);
	//	------------

//		パスワードが一致しない場合
		if(!(password.equals(password2))) {
			request.setAttribute("errMsg", "入力された値が違います。");
			//	jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(UPDATE_USER);
			dispatcher.forward(request, response);
			return;
		}

//		パスワード以外が空の場合
		if(name.equals("") || birth_date.equals("")) {
			request.setAttribute("errMsg", "入力された値が違います。");
			//	jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(UPDATE_USER);
			dispatcher.forward(request, response);
			return;
		}

//-------------------------------------------------------------------------
//		両方が空の場合はパスワード以外を更新
		if(password.equals("") && password2.equals("")) {
			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			UserDao userDao = new UserDao();
			userDao.upDateUser2(name, birth_date, id);

			response.sendRedirect("UserListServlet");
			return;
		}

		// Daoのメソッドを実行
		UserDao userDao = new UserDao();
		userDao.upDateUser(password, name, birth_date, id);

		response.sendRedirect("UserListServlet");

    }

}

