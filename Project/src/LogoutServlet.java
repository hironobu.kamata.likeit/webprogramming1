

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/logout_servlet")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String USER_LIST = "/WEB-INF/jsp/user-list.jsp";

    public LogoutServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		// ログイン時に保存したセッション内のユーザ情報を削除
		session.removeAttribute("userInfo");
		// ログインのサーブレットにリダイレクト
		response.sendRedirect("login_servlet");
    }

}
