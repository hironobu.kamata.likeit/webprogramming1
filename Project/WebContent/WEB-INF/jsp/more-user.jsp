<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー詳細</title>
</head>
<body>

	<div>
		<p>${userInfo.name} さん</p>
		<a href="logout_servlet">ログアウト</a>
	</div>

	<h1>ユーザー詳細情報参照</h1>

	<dl>
		<dt>ログインID</dt>
		<dd>${userDate.login_id}</dd>

		<dt>ユーザー名</dt>
		<dd>${userDate.name}</dd>

		<dt>生年月日</dt>
		<dd>${userDate.birth_date}</dd>

		<dt>登録日時</dt>
		<dd>${userDate.create_date}</dd>

		<dt>更新日時</dt>
		<dd>${userDate.update_date}</dd>
	</dl>

	<a href="UserListServlet">戻る</a>

</body>
</html>