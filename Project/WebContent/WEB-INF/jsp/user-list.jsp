<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link rel="stylesheet" href="./css/user-list.css">
</head>
<body>

	<div>
		<p>${userInfo.name} さん</p>
		<a href="logout_servlet">ログアウト</a>
	</div>

	<h1>ユーザー一覧</h1>

	<a href="NewUserServlet">新規登録</a>

	<form action="UserListServlet" method="post">
	  <div>
	    <label>ログインID</label>
	    <input type="text" name="login_id">
	  </div>

	  <div>
	    <label>ユーザー名</label>
	    <input type="text" name="name">
	  </div>

	  <div>
	    <label>生年月日</label>
	    <input type="text" name="date1"> ~ <input type="text" name="date2">
	  </div>

	  <button type="submit">検索</button>
	</form>

 	<table>
		<tr>
			<th>ログインID</th>
			<th>ユーザー名</th>
			<th>生年月日</th>
			<th></th>
		</tr>

		<!-- 情報 -->
		<c:forEach var="user" items="${userList}">
	<tr>
				<th>${user.login_id}</th>
				<th>${user.name}</th>
				<th>${user.birth_date}</th>
		<th>

　	 	<c:if test="${userInfo.login_id == 'admin' && user.login_id != 'admin'}">
				<a id="more" href="MoreUserServlet?id=${user.id}">詳細</a>
				<a id="update" href="UpdateUserServlet?id=${user.id}">更新</a>
				<a id="delete" href="DeleteServlet?id=${user.id}">削除</a>
		</c:if>

		<c:if test="${userInfo.login_id != 'admin' && userInfo.login_id == user.login_id}">
			<a id="update" href="UpdateUserServlet?id=${user.id}">更新</a>
		</c:if>

		<c:if test="${userInfo.login_id != 'admin' && user.login_id == user.login_id}">
			<a id="more" href="MoreUserServlet?id=${user.id}">詳細</a>
		</c:if>
		</th>
	</tr>
	</c:forEach>
	</table>

</body>
</html>

