
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet" href="./css/common.css">
</head>
<body>

	<h1>ログイン画面</h1>
	<c:if test="${errMsg != null}" >
	    <div class="text">
		  ${errMsg}
		</div>
	</c:if>

	<form action="login_servlet" method="post">
	  <div>
	    <label>ログインID</label>
	    <input type="text" name="login_id">
	  </div>

	  <div>
	    <label>パスワード</label>
	    <input type="password" name="password">
	  </div>

	  <button type="submit" value="送信">ログイン</button>
	</form>


</body>
</html>