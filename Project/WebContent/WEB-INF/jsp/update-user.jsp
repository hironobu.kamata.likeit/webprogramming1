<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>更新情報更新</title>
<link rel="stylesheet" href="./css/common.css">
</head>
<body>

	<div>
		<p>${userInfo.name} さん</p>
		<a href="logout_servlet">ログアウト</a>
	</div>

	<h1>ユーザー情報更新</h1>
	<c:if test="${errMsg != null}" >
	    <div class="text">
		  ${errMsg}
		</div>
	</c:if>


	<form action="UpdateUserServlet" method="post">
	<input type="hidden" name="id" value="${userDate.id}">

	  <div>
	    <label>ログインID</label>
	    <input type="text" name="login_id" value="${userDate.login_id}" readonly>
	  </div>

	  <div>
	    <label>パスワード</label>
	    <input type="password" name="password">
	  </div>

	  <div>
	    <label>パスワード(確認)</label>
	    <input type="password" name="password2">
	  </div>

	  <div>
	    <label>ユーザー名</label>
	    <input type="text" name="name" value="${userDate.name}">
	  </div>

	  <div>
	    <label>生年月日</label>
	    <input type="text" name="birth_date" value="${userDate.birth_date}">
	  </div>

	  <button type="submit">更新</button>
	</form>


	<a href="UserListServlet">戻る</a>

</body>
</html>