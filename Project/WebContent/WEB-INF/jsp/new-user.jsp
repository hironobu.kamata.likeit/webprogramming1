<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録</title>
<link rel="stylesheet" href="./css/common.css">
</head>
<body>

	<div>
		<p>${userInfo.name} さん</p>
		<a href="logout_servlet">ログアウト</a>
	</div>

	<h1>新規登録</h1>
	<c:if test="${errMsg != null}" >
	    <div class="text">
		  <p>${errMsg}</p>
		</div>
	</c:if>

	<form action="NewUserServlet" method="post">
	  <div>
	    <label>ログインID</label>
	    <input type="text" name="login_id">
	  </div>

	  <div>
	    <label>パスワード</label>
	    <input type="password" name="password">
	  </div>

	  <div>
	    <label>パスワード(確認)</label>
	    <input type="password" name="password2">
	  </div>

	  <div>
	    <label>ユーザー名</label>
	    <input type="text" name="name">
	  </div>

	  <div>
	    <label>生年月日</label>
	    <input type="text" name="birth_date">
	  </div>

	  <button type="submit" value="登録">登録</button>
	</form>

	<a href="UserListServlet">戻る</a>

</body>
</html>