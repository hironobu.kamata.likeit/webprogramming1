

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String DELETE_USER = "/WEB-INF/jsp/delete.jsp";

    public DeleteServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// ------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("login_servlet");
			return;
		}
// ------------------------------------------------------------------
		int id = Integer.parseInt(request.getParameter("id"));

		UserDao userDao = new UserDao();
		User userDate = userDao.findByMoreList(id);

		HttpSession session2 = request.getSession();
		session2.setAttribute("userDate", userDate);

		RequestDispatcher dispatcher = request.getRequestDispatcher(DELETE_USER);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int id = Integer.parseInt(request.getParameter("id"));

//		リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		userDao.deleteUser(id);

		response.sendRedirect("UserListServlet");

	}

}

